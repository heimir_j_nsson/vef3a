<?php

	class Bok{

		public $titill;
		public $utgefandi;
		public $ar;
		public $lysing;

		public function __construct($_tit,$_utg, $_ar, $_lys)
		  {
		      $this->titill = $_tit;
		      $this->utgefandi = $_utg;
		      $this->ar = $_ar;
		      $this->lysing = $_lys;
		  } 
	
	}

// model
	class Model{

    public $safn;
    public $mode = 0;
    public $details;

	public function __construct(){

		$this->safn = array();

		$this ->safn[] =  new Bok('Englar Alheimsins','Almenna bókafélagið', '1993','Fáar sögur hafa hitt íslensku þjóðina jafnrækilega í hjartastað og saga Páls, allt frá draumi mömmu hans nóttina áður en hann fæddist og þar til yfir lýkur, enda er hún gædd einstakri hlýju og húmor.');
		
		$this ->safn[] =  new  Bok('Benjamín Dúfa','Vaka-Helgafell' ,'1992','Verðlaunabókin Benjamín dúfa segir frá viðburðaríku sumri í litlu hverfi. Þegar hrekkjusvínið Helgi svarti fremur enn eitt illvirkið ákveða fjórir vinir að taka höndum saman, stofna Reglu rauða drekans og hefja baráttu gegn ranglæti heimsins.');

		$this ->safn[] =  new Bok('Tár, Bros og Takkaskór', 'Fróði hf' ,'1990',' Kiddi og Tryggvi eru leiddir inná sviðið, heilbrigðir, elskulegir snáðar, á hvolpaaldri en með karlagrobb á vörum. Lífið er fótbolti og frami á þeirri braut.');
		
		$this ->safn[] =  new Bok('Svartfugl', 'Bjartur' ,'1971','Svartfugl er magnað skáldverk og talin af mörgum albesta bók Gunnars Gunnarssonar. Bakgrunnurinn eru Sjöundármorðin, einhver umtöluðustu morðmál Íslandssögunnar, en jafnframt er dregin upp skörp mynd af íslensku samfélagi um aldamótin 1800.');
	}	

  public function setDetails($tit){
   $this->mode = 1;
   foreach ($this->safn as $bok) {
    if ($bok -> titill == $tit ) {
      $this->details = $bok;
    }

   }

  }
				
	}                       
?>  