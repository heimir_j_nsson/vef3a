create database eldhus;

-- töflur --
create table Notendur(
id int(11) not null auto_increment primary key,
nafn varchar(45) not null,
email varchar(45) not null,
lykilorð varchar(45) not null
);

create table Vörur(
id int(11) not null auto_increment primary key, 
nafn varchar(45) not null,
flokkur int(11) not null, -- kælivarar eða stofuhiti
tegund varchar(45) not null, -- kjöt fyskur grænmeti matarolía
form varchar(45) null -- gúllas/hakk 
);

create table Notandavörur(
notandi int(11) not null,
vara int(11) not null,
magn varchar(45) not null,
foreign key Fk_notandi(notandi) references Notendur(id),
foreign key Fk_kælivörur(vara) references Vörur(id)
);
-- View --
delimiter $$
 
create procedure ViewMatur()
begin
	Drop view  if exists matur;
    
	CREATE VIEW matur AS SELECT 
	a.id AS ID,a.nafn as Nafn, b.nafn AS Vara , c.magn AS Magn FROM
    Notendur a, Vörur b, Notandavörur c WHERE a.id = c.notandi AND b.id = c.vara; 
end$$

delimiter $$
 
create procedure InsertMatur(I_notandi int(11), I_vara int(11), I_magn varchar(45))
begin
	insert into `Notandavörur`(`notandi`,`vara`,`magn`) values(I_notandi,I_vara,I_magn);  
end$$

delimiter $$
 
create procedure InsertVörur(I_nafn varchar(45), I_flokkur int(11), I_tegund varchar(45), I_form varchar(45) )
begin
	insert into `Vörur`(`nafn`,`flokkur`,`tegund`,`form`) values(I_nafn,I_flokkur,I_tegund,I_form);  
end$$

delimiter $$
 
create procedure UpdateMatur(U_notandi int(11), U_vara int(11), U_magn varchar(45))
begin
	Update `Notandavörur` Set `notandi` = U_notandi,`vara` = U_vara,`magn` = U_magn WHERE `notandi` =  U_notandi AND `vara`= U_vara;  
end$$

delimiter $$

create procedure DelMatur(D_notandi int(11), D_vara int(11))
begin
	DELETE FROM `Notandavörur` WHERE `notandi` = D_notandi and `vara`= D_vara;  
end$$

 -- insert Notendur--
INSERT INTO `Notendur`(`id`, `nafn`, `email`, `lykilorð`) VALUES (1,'admin','admin@eldhus.org','joimattos');
INSERT INTO `Notendur`(`id`, `nafn`, `email`, `lykilorð`) VALUES (2,'tester','tester@eldhus.org','1234');
INSERT INTO `Notendur`(`id`, `nafn`, `email`, `lykilorð`) VALUES (3,'Joi','admin@eldhus.org','joimattos');
INSERT INTO `Notendur`(`id`, `nafn`, `email`, `lykilorð`) VALUES (4,'Heimir','tester@eldhus.org','1234');

-- insert Vörur-- 
INSERT INTO `Vörur`(`id`, `nafn`, `flokkur`, `tegund`,`form`) VALUES (1,'Svínakjöt',1,'kjöt','gúllas');
INSERT INTO `Vörur`(`id`, `nafn`, `flokkur`, `tegund`,`form`) VALUES (2,'Tómatsósa',2,'sósa','');
INSERT INTO `Vörur`(`id`, `nafn`, `flokkur`, `tegund`,`form`) VALUES (3,'Nýmjólk',1,'Mjólkurvörur','');

-- insert Notendavörur
INSERT INTO `Notandavörur`(`notandi`, `vara`, `magn`) VALUES (2,1,'2 kg');
INSERT INTO `Notandavörur`(`notandi`, `vara`, `magn`) VALUES (2,2,'2 stk');
INSERT INTO `Notandavörur`(`notandi`, `vara`, `magn`) VALUES (2,3,'6 L');





