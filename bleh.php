<!DOCTYPE html>

<html>
<head>
 <title>JSON Parsing</title>
 <style>
		h1 {
		    border-bottom: 3px solid #cc9900;
		    color: #996600;
		    font-size: 30px;
		}
		
	</style>
</head>
<body>
	<h1></h1>
		<div>
			<?php

			$events = array(array('location' => 'San Francisco,CA','Austin,TX','New York,NY'),array('date' => 'May l','May 15','May 30'),array('map' => 'img/map-ca.png','img/map-tx.png','img/map-ny.png',));
			$events = json_encode($events);

			echo "<pre>",$events[1]->location,"</pre>";
			print_r($events[1]->date);
			echo $events;

			$json = '{"events":[{"location":"San Francisco,CA","date":"May l","map":"img/map-ca.png"},{"location":"Austin,TX","date":"May 15","map":"img/map-tx.png"},{"location":"New York,NY","date":"May 30","map":"img/map-ny.png"}]}';
			
			var_dump(json_decode($json, true));			

			echo "<pre>",$json[0]->location,"</pre>";
			print_r($json[0]->date);
			echo $json;

			           
			?>

		</div>

</body>
</html>