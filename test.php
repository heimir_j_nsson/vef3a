<html>

<head>
	<style>
		h1 {
		    border-bottom: 3px solid #cc9900;
		    color: #996600;
		    font-size: 30px;
		}
		table, th , td {
		    border: 1px solid grey;
		    border-collapse: collapse;
		    padding: 5px;
		}
		table tr:nth-child(odd) {
		    background-color: #f1f1f1;
		}
		table tr:nth-child(even) {
		    background-color: #ffffff;
		}
	</style>
</head>

<body>
		<h1>Currency exchange</h1>
		<div>
<?php

 $url ="http://apis.is/currency/arion";
            
            $json = file_get_contents($url);
            $json = json_decode($json);
    
            
            echo "<table>";
            echo "<tr><td>", "Country", "</td>","<td>", "Buy", "</td>","<td>", "Sell", "</td>","<td>", "Change", "</td></tr>";
            	        
             foreach ($json->results as $key => $value) {
            	echo "<tr><td>", $value->shortName, "</td>","<td>", $value->askValue, "</td>","<td>", $value->bidValue, "</td>","<td>", $value->changeCur, "</td></tr>";
            	           	
            }
            
            echo"</table>";
           
?>

		</div>
	</body>
</html>