<?php

// GET handler
$app->get('/innkaup', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/innkaup' route");

    // Náum í allar vörurnar
    $sth = $this->db->prepare("SELECT * FROM Vörur");
    $sth->execute();
    $vorur = $sth->fetchAll();

     $sth2 = $this->db->prepare("SELECT Id,Vara, Magn FROM matur where ID = 2 ;");
        $sth2->execute();
        $listi = $sth2->fetchAll();

    // Render index view
    return $this->renderer->render($response, 'innkaup.phtml', ["vorur" => $vorur,'listi' => $listi]);
});

// POST handler for adding vara
$app->post('/innkaup/add', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/innkaup/add' route ");

    $vara['vnotandi'] = 2;
    $vara['vvara'] = $request->getParam('vvara');
    $vara['vmagn'] = $request->getParam('vmagn');    

    if (!is_numeric($vara['vvara']) || $vara['vmagn'] === " ") {           
         return $this->renderer->render($response, 'innkaup.add.phtml', ["vara" => $vara['vvara']]);
    } 
    else {
        $sth = $this->db->prepare("CALL InsertMatur(:notandi, :vara, :magn)");
        $sth->bindParam(':notandi', $vara['vnotandi'], PDO::PARAM_INT);
        $sth->bindParam(':vara', $vara['vvara'], PDO::PARAM_INT);
        $sth->bindParam(':magn', $vara['vmagn'], PDO::PARAM_STR);
        $sth->execute(); 
        
        }
    
       
    // Render index view
    return $this->renderer->render($response, 'innkaup.add.phtml', ["vara" => $vara['vvara']]);
});

$app->get('/innkaup/delete/{id}', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/innkaup/delete' route with $id");

    $id = $request->getAttribute('id');

    $sth = $this->db->prepare('SELECT vara FROM Notandavörur WHERE notandi = 2 AND vara = :id');
    $sth->bindParam(':id', $id, PDO::PARAM_INT);
    $sth->execute();
    $nafn = $sth->fetchColumn();

    // Render the view
    return $this->renderer->render($response, 'vorur.delete.phtml', ["notandi" => $nafn, "id" => $id]);
});

$app->post('/innkaup/delete', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/innkaup/delete' route");
    
    $id = $request->getParam('id');
    
    // Call procedure to delete
    $sth = $this->db->prepare("CALL DelMatur(2,:id)");
    $sth->bindParam(':id', $id, PDO::PARAM_INT);
    $sth->execute(); 


    // Render index view
    return $this->renderer->render($response, 'vorur.html', $args);
});


