<?php

// GET handler
$app->get('/vorur', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/vorur' route");

    // Náum í allar vörurnar

    $sth = $this->db->query('SELECT * FROM Vörur');
    $sth->execute();
    $vorur = $sth->fetchAll();

    // Render the view
    return $this->renderer->render($response, 'vorur.phtml', ["vorur" => $vorur]);
});

// POST handler for adding vara
$app->post('/vorur/add', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/vorur/add' route");

    $vara['vnafn'] = $request->getParam('vnafn');
    $vara['vflokkur'] = $request->getParam('vflokkur');
    $vara['vtegund'] = $request->getParam('vtegund');
    $vara['vform'] = $request->getParam('vform');

    // Call procedure to add
	$sth = $this->db->prepare("CALL InsertVörur(:nafn, :flokkur, :tegund, :form)");
	$sth->bindParam(':nafn', $vara['vnafn'], PDO::PARAM_STR);
	$sth->bindParam(':flokkur', $vara['vflokkur'], PDO::PARAM_INT);
	$sth->bindParam(':tegund', $vara['vtegund'], PDO::PARAM_STR);
	$sth->bindParam(':form', $vara['vform'], PDO::PARAM_STR);
	$sth->execute(); 

    // Render the view
    return $this->renderer->render($response, 'vorur.add.phtml', ["nafn" => $vara['vnafn']]);
});

$app->get('/vorur/delete/{id}', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/vorur/delete' route with $id");

    $id = $request->getAttribute('id');

    // Við vinnum bara með tölur!
    if(!is_numeric($id))
    {
    	echo "verður að vera tala!";
    	return;
    }

    $sth = $this->db->prepare('SELECT nafn FROM Vörur WHERE id = :id');
	$sth->bindParam(':id', $id, PDO::PARAM_INT);
    $sth->execute();
    $nafn = $sth->fetchColumn();

    // Render the view
    return $this->renderer->render($response, 'vorur.delete.phtml', ["nafn" => $nafn, "id" => $id]);
});

$app->post('/vorur/delete', function ($request, $response, $args) {
    // Sample log message
    $this->logger->info("Slim-Skeleton '/vorur/delete' route");
    
    $id = $request->getParam('id');
    
    // Call procedure to delete
	$sth = $this->db->prepare("CALL DelVörur(:id)");
	$sth->bindParam(':id', $id, PDO::PARAM_INT);
	$sth->execute(); 


    // Render index view
    return $this->renderer->render($response, 'vorur.html', $args);
});

